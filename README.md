# Golang + GCE

## 方法一 (直接使用GCE+GCR)

1. 本地Clone靜態網站專案

   ```bash
   git clone https://gitlab.com/KennyChenFight/gce-golang-demo.git
   ```

2. 本地docker build 

   ```bash
   cd gce-golang-demo
   docker build . -t gce-golang-demo
   ```

3. tag docker image

   ```bash
   docker tag gce-golang-demo gcr.io/<project_id>/gce-golang-demo:v1
   ```

4. push image to gcr

   ```bash
   docker push gcr.io/<project_id>/gce-golang-demo:v1
   ```

5. 建立VM並指定container

   ```bash
   gcloud compute instances create-with-container <vm_name> --container-image gcr.io/<project_id>/gce-golang-demo:v1 --tags http-server --zone [your_zone]
   ```

6. 開啟VM的External IP 即可看到~

### 如何更新VM上的容器?

先push到gcr上，接著利用指令更新

```bash
gcloud compute instances update-container <vm_name> \
    --container-image gcr.io/<project_id>/gce-golang-demo:<version>
```



