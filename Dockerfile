FROM golang:1.13-alpine
WORKDIR /gce-golang-demo
ADD . /gce-golang-demo
RUN cd /gce-golang-demo && go build
ENTRYPOINT ["./gce-golang-demo"]